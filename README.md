# Kooka messenger

Communication between services by using Nats

### Examples:

```rust
use kooka_lib_messenger::sender;

if let Err(e) = sender::produce_message(&data, &format!("{}.{}", SUBJECT_BASE, "test_request")) {
    println!("Failed producing messages: {}", e);
}
```

```rust
use kooka_lib_messenger::receiver;

match receiver::subscribe("some_route", None) {
    Ok(client) => {
        println!("Connected....");
        let message = receiver::consume_messages(client).unwrap();
        println!("Message: ", message);
    },
    Err(e) => {
        println!("Subscribe error: {}", e);
    }
}
```
