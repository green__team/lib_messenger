extern crate nats;

use nats::Client;

use std::net::ToSocketAddrs;
use std::net::IpAddr;

fn resolve(host: &str) -> Result<String, &'static str> {
    match (host, 0).to_socket_addrs() {
        Ok(ips) => {
            let ips: Vec<IpAddr> = ips.map(|address| address.ip()).collect();
            if ips.len() > 0 {
                let ip = ips[0].to_string();
                Ok(ip)
            } else {
                println!("Error lookup host - host not found");
                Err("Error lookup host - host not found")
            }
        },
        Err(e) => {
            println!("Error lookup host {}",e);
            Err("Error lookup host")
        }
    }
}

pub struct MessengerEvent {
  pub msg: String,
  pub subject: String,
  pub inbox: Option<String>,
}

#[derive(Debug)]
pub struct MessengerClient {
    client: Client,
}

impl MessengerClient {
    pub fn new(hosts: String, name: String) -> Result<MessengerClient, &'static str> {
        let hosts_parsed: Vec<String> = hosts.split(',')
            .map(|host| {
                let host = String::from(host);
                let len = host.len();
                let beta_offset = host.find(':').unwrap_or(len);
                let port = host.get(beta_offset..len).unwrap_or_default();
                let host = host.get(0..beta_offset).unwrap_or_default();

                format!("nats://{}{}", resolve(&host).unwrap_or_default(), port)
            })
            .collect();
        println!("Trying to connect to -> {:?}", hosts_parsed);
        match Client::new(hosts_parsed) {
            Ok(mut client) => { 
                println!(".....Connected!");
                client.set_name(&name);
                Ok(MessengerClient {
                    client,
                })
            },
            Err(e) => {
                println!("Connection to Nats is failed: {}", e);
                return Err("Connection to Nats is failed");
            },
        }
    }

    pub fn subscribe(&mut self, subject: &str, queue: Option<&str>) -> Result<(), &'static str> {
        if let Err(e) = self.client.subscribe(subject, queue) {
            println!("Subscribe is failed! {}", e);
            return Err("Subscribe is failed!");
        } else {
            return Ok(());
        }
    }

    pub fn consume_messages(&mut self) -> Result<MessengerEvent, &'static str> {
        match self.client.wait() {
            Ok(event) => {
                let msg = String::from_utf8_lossy(&event.msg).into_owned();

                Ok(MessengerEvent {
                  msg,
                  subject: event.subject,
                  inbox: event.inbox,
                })
            },
            Err(e) => {
                println!("Consume messages is failed! {}", e);
                Err("Consume messages is failed!")
            }
        }
    }

    pub fn produce_message(&mut self, subject: &str, data: &str) -> Result<(), &'static str> {
        println!("About to publish a message to: {}", subject);
        if let Err(e) = self.client.publish(subject, data.as_bytes()) {
            println!("Publish error {}", e);
            return Err("Publish error!");
        } else {
            return Ok(());
        }
    }

    pub fn rpc_call(&mut self, subject: &str, data: &str) -> Result<MessengerEvent, &'static str> {
        println!("About to rpc call to: {}", subject);
        match self.client.make_request(subject, data.as_bytes()) {
            Ok(inbox) => {
                if let Err(e) = self.subscribe(&inbox, None) {
                    println!("Rpc subscribe error {}", e);
                    return Err("Rpc subscribe error!");
                } else {
                    match self.consume_messages() {
                        Ok(res) => {
                            Ok(res)
                        },
                        Err(e) => {
                            println!("Rpc consume messages error {}", e);
                            return Err("Rpc consume messages error!");
                        }
                    }
                }
            },
            Err(e) => {
                println!("Make Rpc request is failed! {}", e);
                Err("Make Rpc request is failed!")
            }
        }
    }
}
